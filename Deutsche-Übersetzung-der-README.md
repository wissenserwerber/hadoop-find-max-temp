# hadoop-find-max-temp

Eine Demonstration der ersten zwei Beispielen im [Hadoop Book](http://hadoopbook.com) Tom Whites. Sie kriegt und bearbeitet alle Klimadaten from aus NCDC (das National Climatic Data Center der USA) zwischen 1900 und 1955 und findet die größte Temperatur des jeden Jahrs. Das Skript zum Kriegen der jahrlichen Klimadaten basiert sich auf [ncdc.sh](https://gist.github.com/Alexander-Ignatyev/6478289) des Alexander Ignatyevs

Das Shell-Skript, das das klassische Unix-Werkzeug 'awk' enthält ist zum Vergleich zu Hadoop eingeschlossen. Das Rechnen mit dem 'awk' Skript dauert 12:30 Minuten wo es mit dem Hadoop mit History Server 13:43 Minuten dauert.

![Unix Processing Time](http://omeroztat.com/img/ProcessingTimeUsingUnix.png)
 ![Unix Processing Time](http://omeroztat.com/img/ProcessingTimeUsingHadoop.png)

# Das Kriegen der Jahrlichen Klimadaten:

$ cd ~/hadoop/big_data_folder

$ ./get-and-process-NCDC-data.sh 1901 1955

Es wird ein Verzeichnis namens ncdc_big_data erstellt und darunter werden sich die Klimadaten zwischen 1900 und 1955 befinden.

# Haddop Classpath Feststellen (zum Laufen der Hauptanwendung, MaxTemperature.class benötigt)

$ export HADOOP_CLASSPATH=~/hadoop/find-max-temperature/lib/hadoop-examples.jar


# Compile:

$ javac -cp $(hadoop classpath):$(~/hadoop/hadoop-find-max-temp/hadoop-examples.jar)  -d ../../classes MaxTemperature.java

or (depending on your directory structure)

$ javac -cp $(hadoop classpath):/Users/uname/hadoop/hadoop-book/hadoop-examples.jar -d ../../../target MaxTemperature.java

$ start-dfs.sh

$ start-yarn.sh

$ $HADOOP_HOME/sbin/mr-jobhistory-daemon.sh start historyserver

# Put big data to Hadoop's file system folder:

$ hadoop fs -mkdir -p /input/ncdc

$ hadoop fs -put ~/hadoop/big_data_folder/ncdc_big_data/* /input/ncdc/

# Run:

Before running:

$ hadoop fs -rm -r output (Any output folders need to cleaned otherwise it halts)

Run: (i.e. start the Map-Reduce job)

$ hadoop MaxTemperature input/ncdc/* output
