# hadoop-find-max-temp

A demo of the example in Tom White's [Hadoop Book](http://hadoopbook.com). It gets and processes all temperature data from NCDC (US National Climatic Data Center) between 1900 - 1955 and finds the biggest temperature for each year. The script to get and process yearly climate data is **get-and-process-NCDC-data.sh** and is based on Alexander Ignatyev's [ncdc.sh](https://gist.github.com/Alexander-Ignatyev/6478289)

The shell script which involves the Unix tool *awk* is included in file **find-max-temperature.sh** for comparison. It takes 12:30 Minutes with *awk* while it takes 13:43 Minutes Hadoop with History Server.

 ![Unix Processing Time](img/ProcessingTimeUsingUnix.png)
 ![Unix Processing Time](img/ProcessingTimeUsingHadoop.png)

## To get and process the yearly data:

$ ./get-and-process-NCDC-data.sh 1901 1955

A folder named ncdc_big_data will be created and inside will be all data between 1900  1955.

##  Specify Hadoop Home and Classpath (to run the main application MaxTemperature.class is needed)

$ export HADOOP_HOME=/usr/local/hadoop # Angenommen dass hadoop-2.x symlinkt ist
$ export HADOOP_CLASSPATH=~/hadoop/find-max-temperature/classes
 
## Compile:

$ javac -cp $(hadoop classpath):$(~/hadoop/hadoop-find-max-temp/hadoop-examples.jar)  -d ../../classes MaxTemperature.java

or (depending on your directory structure)

$ javac -cp $(hadoop classpath):/Users/uname/hadoop/hadoop-book/hadoop-examples.jar -d ../../../target MaxTemperature.java

## Start Hadoop with History Server

add those lines to mapred-site.xml

    <property>
      <name> mapreduce.framework.name</name>
      <value>yarn</value>
    </property>
    <property> 
      <name>mapreduce.jobhistory.address</name>
      <value>localhost:10020</value> 
    </property>
    <property> 
      <name>mapreduce.jobhistory.webapp.address</name>
     <value>localhost:19888</value> 
    </property>

add those to yarn-site.xml

    <property>
         <name>yarn.log-aggregation-enable</name>
      <value>true</value>
      </property>
      <property>
         <name>yarn.nodemanager.remote-app-log-dir</name>
         <value>/app-logs</value>
      </property>
      <property>
          <name>yarn.nodemanager.remote-app-log-dir-suffix</name>
          <value>logs</value>
      </property>

add this to yarn-site.xml, again:

    <property>
      <name>yarn.nodemanager.aux-services</name>
      <value>mapreduce_shuffle</value>
    </property>

$ start-dfs.sh

$ start-yarn.sh

$ $HADOOP_HOME/sbin/mr-jobhistory-daemon.sh start historyserver

## Run

### Initialization of Hadoop directories and files to be processed and output dir

$ hadoop fs -mkdir /user/macmini
$ hdfs dfs  -ls    /user/macmini
$ hadoop fs -mkdir -p /user/macmini/input/ncdc
$ hadoop fs -put ncdc/ncdc_big_data/*.gz ~/input/ncdc/

### For each new start

$ hadoop fs -rm -r output/* (Output folder need to cleaned to function properly)

Run: (start the Map-Reduce job)

$ hadoop fs -ls /user/hadoop-user/
$ hadoop fs -mkdir /user/hadoop-user/output

$ export HADOOP_CLASSPATH=~/hadoop/hadoop-find-max-temp/classes
$ hadoop MaxTemperature input/ncdc/* output

Go to http://localhost:8088/cluster  and thereunder an entry like application_1539078685552_0001	will be seen. Click on it and the MapReduce applications status will be displayed where you can monitor the time it takes to complete the job.

#### Ref git @ gitlab.com : wissenserwerber/hadoop-find-max-temp.git 
