#!/usr/bin/env bash
start=$(date +"%s")

for year in ncdc/ncdc_big_data/*
do
    echo -ne `basename $year .gz`"\t"
    gunzip -c $year | \
	awk '{ temp = substr($0, 88, 5) + 0;
               q = substr($0, 93, 1);
               if (temp !=9999 && q ~ /[01459]/ && temp > max) max = temp }
            END { print max }'   
done
finish=$(date +"%s")
echo "It took $(($finish+1-$start-1)) seconds to complete"
