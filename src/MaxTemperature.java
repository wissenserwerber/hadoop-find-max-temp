// cc MaxTemperature Application to find the maximum temperature in the weather dataset
// vv MaxTemperature
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapred.TaskReport;
import org.apache.hadoop.mapred.RunningJob;

import java.util.Date;

public class MaxTemperature {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {

	System.err.println("Usage: MaxTemperature <input path> <output path>");
      System.exit(-1);
    }
    long start = new Date().getTime();
    Job job = new Job();
    boolean status = job.waitForCompletion(true);
    job.setJarByClass(MaxTemperature.class);
    job.setJobName("Max temperature");

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    
    job.setMapperClass(MaxTemperatureMapper.class);
    job.setReducerClass(MaxTemperatureReducer.class);

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    long end = new Date().getTime();
    System.out.println("It took " + (end - start) + " to complete");
    System.exit(job.waitForCompletion(true) ? 0 : 1);    
  }
}
